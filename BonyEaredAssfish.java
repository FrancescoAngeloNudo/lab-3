public class BonyEaredAssfish{
	
	public String Name;
	public String Type;
	public double Size;
	public String Fact;
	public int Depth;
	public String Region;
	
	
	public void eat(){
		System.out.println("The " +Name+ " ate some smaller fish and algae");
	}
	
	public void dive (){
		System.out.println("The Bony-Eared Assfish dove to a depth of " +Depth+ " meteres deep");
	}
}