public class Application{
	public static void main (String[]args){
		
		BonyEaredAssfish assfish = new BonyEaredAssfish();
		
		assfish.Name = "Bony-Eared Assfish";
		assfish.Type = "Cusk Eel";
		assfish.Size = 37.5;
		
		BonyEaredAssfish favfish = new BonyEaredAssfish();
		
		favfish.Fact = "Smallest brain to bodyweight ratio of all vertibrates";
		favfish.Depth = 4415;
		favfish.Region = "Cavernous areas as far north as BC";
		
		System.out.println(assfish.Name);
		System.out.println(favfish.Fact);
		assfish.eat();
		favfish.dive();
		
		BonyEaredAssfish[] swarm = new BonyEaredAssfish[3];
		swarm[0] = assfish;
		swarm[1] = favfish;
		
		swarm[2] = new BonyEaredAssfish();
		swarm[2].Type = "white";
		swarm[2].Name = "Spud";
		swarm[2].Fact = "dumb fish";
		
		System.out.println(swarm[2].Fact);
	}
}